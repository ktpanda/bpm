Brain Password Manager
======================

BPM is an application which will help you remember your passwords.

Requirements
------------

[Python 3.4+](https://www.python.org/downloads/)

Source
------

[Gitlab](https://gitlab.com/ktpanda/bpm/)

Rationale
---------

Most password managers are based on the same basic idea - you remember one password, you
put that into your password manager, and it spits out your plaintext passwords for all the
sites you visit. This is fine for sites you don't visit a lot and don't need high security
for.

However... What happens if you don't have access to your password manager? What happens if
your master password is compromised? What if you're paranoid enough that you just want to
*know* your passwords and not store them where they can be compromised in the first place?

That's what BPM helps you do. When you enter a password into BPM, it does not store the
password. Instead, it runs the password through a very large number of calculations
(800,000 rounds of PBKDF2) to arrive at a number between 0 and 4095. This number is the
only information it stores.

When you practice entering your passwords, it does the same calculations. If the resulting
number is different, then your definitely typed your password wrong. But, if you enter any
password at random, there is a 1 in 4096 chance that it will still say you are
correct. This isn't very likely to happen due to a simple typo in your password, though.

What this means is that any attacker trying to extract your passwords will find that lots
of passwords match, and has no way of proving which one is the real password, other than
trying it out on the site that the password is for. In fact, BPM has a built-in feature
which finds false positives for a password to demonstrate just how many passwords an
attacker would have to try.

Installation
------------

You must have Python 3.4+ installed. On most Linux distributions, it should already be
installed. On Windows, you can download it [here](https://www.python.org/downloads/windows/).

There are pre-built binaries checked into the repository. This is to maintain
accountability for them using the cryptographic guarantees that GIT provides. There is
also a file containing SHA-256 checksums of each binary, along with the version of GCC
used to compile it. If you rebuild them with the exact same version of GCC, you should get
identical results. The build script in hasher/build.sh will verify this for you.

To rebuild the binaries on Ubuntu 16.04 / 18.04, you should install the following packages:

```
sudo apt install build-essential gcc-mingw-w64-i686 gcc-mingw-w64-i686
```

Then run:

```
./hasher/build.sh
```

This will remove the pre-built binaries and rebuild all of them, then check to see if
their checksums match. If the checksums do not match, but the compiler versions are the
same, please let me know, because that should not happen!

Using Brain Password Manager
----------------------------

When you launch `bpm.py`, you will be presented with a prompt for a command. You can enter
"h" for a list of commands.

Enter "n" to create a new password. You will be prompted to enter a name for this
password, which will be used to identify it. This can be the name of the site it is for,
or something that hints at it. You will then be prompted to enter the password twice.

Enter "p" to practice entering your passwords. This will prompt you to enter all of your
passwords in a random order. When you have entered all your passwords, it will shuffle the
list again and start over. To exit practice mode, just enter a blank password.

Enter "pd" to practice all passwords for the current date. By default, passwords are
scheduled to be practiced every day, but you can use the "f" command to change the
frequency for your passwords.

Enter "l" to see a list of your passwords. You can list specific groups of passwords by
specifying a group name after "l", e.g. "l group1"

The "d", "f", "g", "ga", "gr", "t", and "bf" commands will prompt you for a password to
operate on. You can select a password by number, name, or you can select a whole group of
passwords by using "!", e.g. "!group1".

Security
--------

The database is stored in a plain-text JSON format. An attacker who reads this file will
be able to see the names you have chosen for your passwords, and will be able to see the
date of the last time you used the "pd" command. If the attacker has a list of passwords
they suspect belong to you, they will be able to rule out most of them.

There is also no defense against keyloggers, because it is pointless - many keyloggers are
very advanced and run with high privileges. It also doesn't do any good to protect against
them here when the browser or other application you use the password with doesn't have
such protection. You should not use this program on any system you suspect may be
compromised.

The helper program locks its data into memory so it never gets written to disk. It also
erases its data from memory before exiting.
