#!/usr/bin/python3
import sys
import re
import ast
import argparse
import traceback

class TagExpr(object):
    def run(self, s):
        pass

class Tag(TagExpr):
    opchr = ''
    def __init__(self, t):
        self.tag = t

    def run(self, s):
        return self.tag in s

    def invert(self):
        return self.invclass(self.tag)

    def reduce(self, s):
        s.add(self.tag)

    def __repr__(self):
        return self.opchr + self.tag

    def __eq__(self, o):
        return type(o) == type(self) and o.tag == self.tag

class NotTag(Tag):
    opchr = '- '
    def run(self, s):
        return self.tag not in s

    def reduce(self, s):
        return True

class And(TagExpr):
    opchr = '&'

    def __init__(self, e):
        self.e = e

    def addtags(self, s):
        for e in self.e:
            e.addtags(s)

    def run(self, s):
        for e in self.e:
            if not e.run(s):
                return False
        return True

    def reduce(self, s):
        for e in self.e:
            tmps = set()
            if not e.reduce(tmps):
                s.update(tmps)
                return False
        return True

    def invert(self):
        return self.invclass([e.invert() for e in self.e])

    def __repr__(self):
        return '%s %s ;' % (self.opchr, ' '.join(repr(e) for e in self.e))

class Or(And):
    opchr = '|'

    def run(self, s):
        for e in self.e:
            if e.run(s):
                return True
        return False

    def reduce(self, s):
        for e in self.e:
            if e.reduce(s):
                return True

class Xor(And):
    opchr = '^'

    def run(self, s):
        cnt = 0
        for e in self.e:
            if e.run(s):
                cnt += 1
        return bool(cmt & 1)

    def reduce(self, s):
        for e in self.e:
            if e.reduce(s):
                return True

    def invert(self):
        newlst = list(self.e)
        newlst[0] = newlst[0].invert()
        return Xor(newlst)

Tag.invclass = NotTag
NotTag.invclass = Tag
And.invclass = Or
Or.invclass = And

SPACES = ' ,\t\r\n'
SPECIAL = SPACES + '|^&-;'
class TagParser:
    def __init__(self, spec):
        self.spec = spec
        self.pos = 0

    def _skip_space(self):
        try:
            while self.spec[self.pos] in SPACES:
                self.pos += 1
        except IndexError:
            pass

    def _get_tag(self):
        self._skip_space()
        opos = self.pos
        try:
            while True:
                ch = self.spec[self.pos]
                if ch in SPECIAL:
                    break
                self.pos += 1
        except IndexError:
            pass
        tag = self.spec[opos : self.pos]
        return tag

    def _parse_single_tag(self):
        v = self._get_tag()
        return Tag(v)

    def _parse_multi(self, cls):
        e = []
        while True:
            self._skip_space()
            ch = self.spec[self.pos]
            if ch == ';':
                break
            e.append(self._parse_tag_expr())
        if len(e) == 1:
            return e[0]
        return cls(e)

    def _parse_tag_expr(self):
        self._skip_space()
        ch = self.spec[self.pos]
        self.pos += 1
        if ch == '-':
            return self._parse_tag_expr().invert()
        elif ch == '&':
            return self._parse_multi(And)
        elif ch == '|':
            return self._parse_multi(Or)
        elif ch == '^':
            return self._parse_multi(Xor)
        else:
            self.pos -= 1
            return self._parse_single_tag()

def parse_tag_expr_or(s):
    return TagParser('|' + s + ';')._parse_tag_expr()

def parse_tag_expr_and(s):
    return TagParser('&' + s + ';')._parse_tag_expr()

parse_tag_expr = parse_tag_expr_or

def _transform_ast(expr):
    if isinstance(expr, ast.BinOp):
        op = expr.op
        if isinstance(op, ast.BitAnd):
            ncls = And
        elif isinstance(op, ast.BitOr):
            ncls = Or
        elif isinstance(op, ast.BitXor):
            ncls = Xor
        else:
            raise SyntaxError('Unknown operator: %r' % op)

        exprs = []
        for subexp in (_transform_ast(expr.left), _transform_ast(expr.right)):
            if type(subexp) is ncls:
                exprs.extend(subexp.e)
            else:
                exprs.append(subexp)

        return ncls(exprs)
    if isinstance(expr, ast.UnaryOp):
        op = expr.op
        if isinstance(op, ast.USub):
            return _transform_ast(expr.operand).invert()
        raise SyntaxError('Unknown operator: %r' % op)

    if isinstance(expr, ast.Name):
        id = expr.id
        if not id.startswith('_'):
           raise SyntaxError('something went wrong...')

        return Tag(id[1:])

    raise SyntaxError('unexpected syntax in expression: %s' % ast.dump(expr))

def parse_ast(text, allow_assign=False):
    text = re.sub(r'(\w+)', r'_\1', text)

    mod = ast.parse(text)
    if not isinstance(mod, ast.Module):
        raise SyntaxError('unexpected return from ast.parse')

    if len(mod.body) != 1:
        raise SyntaxError('expected exactly one expression')

    assign_names = []

    expr = mod.body[0]

    if allow_assign and isinstance(expr, ast.Assign):
        if not all(isinstance(ast.Name, v) for v in expr.targets):
            raise SyntaxError('expected simple name in assignment')
        assign_names = [v.id[1:] for v in expr.targets]
        expr = expr.value
    else:
        if not isinstance(expr, ast.Expr):
            raise SyntaxError('expected expression, not %s' % ast.dump(expr))

        expr = expr.value

    expr = _transform_ast(expr)

    if allow_assign:
        return assign_names, expr
    else:
        return expr

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('spec')
    args = p.parse_args()

    v = parse_ast(args.spec)
    print(repr(v))


if __name__ == '__main__':
    main()
