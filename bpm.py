#!/usr/bin/env python3
import sys
import re
import time
import os
import argparse
import datetime
import subprocess
import traceback
import copy
import tagexpr
from os.path import dirname, basename, join, exists, expanduser
from collections import defaultdict

import json
import random

from binascii import b2a_hex, a2b_hex

VERSION = '1.0'

ITERATIONS = 800000
def _get_binary_dir():
    import platform
    uname = platform.uname()
    syst = uname.system.lower()
    mach = uname.machine.lower()

    if mach == 'i386' or mach == 'i586' or mach == 'i486':
        mach = 'i686'

    suffix = ''
    if syst == 'windows':
        suffix = '.exe'

    bindir = join(dirname(__file__), 'hasher', 'bin', syst, mach)
    if not exists(join(bindir, 'password_hasher' + suffix)):
        print('WARNING: no companion binaries ar available for %s:%s' % (uname.system, uname.machine))
    return bindir, suffix

BINARY_DIR, BINSUFFIX = _get_binary_dir()

def overwrite_rename(src, dest):
    try:
        os.rename(src, dest)
    except FileExistsError:
        try:
            os.unlink(dest)
        except FileNotFoundError:
            pass
        os.rename(src, dest)

def hash_password(salt, iterations, keylen, pw=None):
    input = None
    stdin = None
    if pw:
        stdin = subprocess.PIPE
        input = pw.encode('utf8') + b'\n'
    p = subprocess.Popen([join(BINARY_DIR, 'password_hasher' + BINSUFFIX), salt, str(keylen), str(iterations)], stdin=stdin, stdout=subprocess.PIPE)
    try:
        out, err = p.communicate(input)
        out = out.decode('ascii').strip()
    except KeyboardInterrupt:
        p.wait()
        raise

    if out == 'e:eof':
        raise EOFError

    if out == 'e:intr':
        raise KeyboardInterrupt

    if out == 'e:empty':
        return None

    if out.startswith('k:'):
        return out[2:].lower()

    return None

def read_hashed_password(prompt, salt, iterations, keylen):
    sys.stdout.write(prompt)
    sys.stdout.flush()
    return hash_password(salt, iterations, keylen)

def read_verify_password(salt, iterations, keylen):
    while True:
        newkey = read_hashed_password('new passwd: ', salt, iterations, keylen)
        if not newkey:
            return None

        newkey_verify = read_hashed_password('verify: ', salt, iterations, keylen)
        if newkey == newkey_verify:
            return newkey
        print('passwords do not match')

def add_stats(base, key, data):
    for c in key:
        base = base.setdefault(c, {})

    for k, v in data.items():
        base[k] = base.get(k, 0) + v

def group_filter_inv(grp):
    return lambda v: grp not in v

def group_filter(grp):
    return lambda v: grp in v

class Password:
    def __init__(self, name, jsdict, statdict):
        self.name = name

        self.iterations = jsdict.get('iterations', ITERATIONS)
        self.salt = jsdict.get('salt', None)
        self.key = jsdict.get('key', None)
        self.active = jsdict.get('active', True)
        self.groups = jsdict.get('groups', [])
        self.freq = jsdict.get('freq', 1)
        self.cycle = jsdict.get('cycle', 0)

        self.stats = statdict

    def password_json(self):
        return {
            'iterations': self.iterations,
            'salt': self.salt,
            'key': self.key,
            'active': self.active,
            'groups': self.groups,
            'freq': self.freq,
            'cycle': self.cycle,
        }

    def __repr__(self):
        return 'pw:%s' % self.name

class PasswordPractice:
    def __init__(self, basepath, stats):
        self.basepath = basepath
        self.use_stats = stats
        self.password_file = join(basepath, 'passwords.json')
        self.stat_file = join(basepath, 'stats.json')

    def load(self):
        try:
            with open(self.password_file, 'r') as fp:
                password_data = json.load(fp)

        except FileNotFoundError:
            password_data = {'passwords': {}}

        try:
            with open(self.stat_file, 'r') as fp:
                self.stats = json.load(fp)
        except FileNotFoundError:
            self.stats = {'passwords': {}, 'cycle': {}}

        pw = self.passwds = {}
        for name, jsdict in password_data['passwords'].items():
            statdict = self.stats['passwords'].get(name)
            if statdict is None:
                self.stats['passwords'][name] = statdict = {}
            pw[name] = Password(name, jsdict, statdict)

        if self.rebalance_passwords():
            self.save()

    def save(self):
        password_data = {'passwords': {pw.name: pw.password_json() for pw in self.passwds.values()}}
        with open(self.password_file + '~', 'w') as fp:
            json.dump(password_data, fp, indent=True, sort_keys=True)

        overwrite_rename(self.password_file + '~', self.password_file)

    def save_stats(self):
        with open(self.stat_file + '~', 'w') as fp:
            json.dump(self.stats, fp, indent=True, sort_keys=True)

        overwrite_rename(self.stat_file + '~', self.stat_file)

    def list_passwords(self, choose_prompt=None, passwords=None):
        lst = sorted((self.passwds.values() if passwords is None else passwords), key=lambda pw: pw.name)

        print('Num  Act Fq / Cy Name            Groups')
        print('---- --- ------- --------------- ---------------')
        for i, pw in enumerate(lst, start=1):
            groups = ' '.join(pw.groups)
            print('%3d:  %s  %2d / %2d %-15s %s' % (i, '*' if pw.active else ' ', pw.freq, pw.cycle, pw.name, groups))

        if choose_prompt:
            while True:
                name = None
                inp = input(choose_prompt)
                if inp == '':
                    return None

                if inp.startswith('!'):
                    return self.filter_password_arg(inp[1:])

                try:
                    idx = int(inp)
                    if 1 <= idx <= len(lst):
                        return [lst[idx - 1]]
                except ValueError:
                    try:
                        return [self.passwds[inp]]
                    except KeyError:
                        pass

                print('Invalid selection')


    def record_stats(self, pw, nstats):
        if not self.use_stats:
            return

        stat = pw.stats

        ctime = int(time.time())
        last_entry = stat.get('last_entry', 0)

        stat['last_entry'] = ctime
        add_stats(stat, (), nstats)

        ctd = datetime.datetime.fromtimestamp(ctime)
        week_key = (ctd - datetime.timedelta((ctd.weekday() + 1) % 7)).strftime('%Y-%m-%d')
        month_key = ctd.strftime('%Y-%m')
        year_key = ctd.strftime('%Y')

        add_stats(stat, ('byweek', week_key), nstats)
        add_stats(stat, ('bymonth', month_key), nstats)
        add_stats(stat, ('byyear', year_key), nstats)
        self.save_stats()

    def start_cycle(self):
        cycle_stats = self.stats['cycle']
        cur_counter = cycle_stats.get('counter', 0)

        cdate = datetime.date.today().strftime('%Y-%m-%d')
        if cdate != cycle_stats.get('last_complete'):
            cur_counter += 1
        return cdate, cur_counter

    def show_cycle(self):
        cycle_stats = self.stats['cycle']
        cdate = datetime.date.today()
        cur_counter = cycle_stats.get('counter', 0)

        while True:
            for j in range(20):
                lst = self.get_cycle_passwords(cur_counter)
                lst.sort(key=lambda pw: (pw.freq, pw.name))
                print('%s: %5d: %s' % (cdate.strftime('%Y-%m-%d'), cur_counter, ' '.join(pw.name for pw in lst)))
                cdate += datetime.timedelta(days=1)
                cur_counter += 1

            txt = input('press enter for more, or (q) to quit.')
            if txt == 'q':
                break

    def get_cycle_passwords(self, counter=None):
        if counter is None:
            cycle_stats = self.stats['cycle']
            counter = cycle_stats.get('counter', 0)

        return [pw for pw in self.passwds.values() if (counter % pw.freq) == pw.cycle and pw.active]

    def rebalance_passwords(self):
        by_freq = defaultdict(list)
        pwlist = list(self.passwds.values())
        random.shuffle(pwlist)
        any_changes = False

        for pw in pwlist:
            if pw.freq != 1:
                by_freq[pw.freq].append(pw)

        for freq, pwlist in by_freq.items():
            bad_cycle = []
            by_cycle = [[] for j in range(freq)]
            expect_cycle_count = (len(pwlist) + freq - 1) // freq
            for pw in pwlist:
                if 0 <= pw.cycle < pw.freq:
                    by_cycle[pw.cycle].append(pw)
                else:
                    bad_cycle.append(pw)

            for pw in bad_cycle:
                idx = list(range(freq))
                random.shuffle(idx)
                for cs in idx:
                    lst = by_cycle[cs]
                    if len(lst) < expect_cycle_count:
                        lst.append(pw)
                        pw.cycle = cs
                        any_changes = True
                        break

            while True:
                longest = max(by_cycle, key=len)
                if len(longest) <= expect_cycle_count:
                    break
                shortest = min(by_cycle, key=len)
                pw = longest.pop()
                pw.cycle = by_cycle.index(shortest)
                any_changes = True
                shortest.append(pw)

        return any_changes

    def bruteforce_password(self, pw):
        try:
            chars = 'abcdefghijklmnopqrstuvwxyz0123456789_'
            print('Finding false positives for "%s", press Ctrl-C to stop' % (pw.name))
            p = subprocess.Popen([join(BINARY_DIR, 'password_bruteforcer' + BINSUFFIX), pw.salt, pw.key, '12', str(pw.iterations), chars])
            p.wait()
        except KeyboardInterrupt:
            p.terminate()
            p.wait()

    def menu(self):
        print('Brain Password Manager v%s' % VERSION)
        print('Enter "h" for help, "q" to quit')
        while True:
            print()

            cmd = input('command? ')
            cmd = cmd.lower().strip()
            if cmd == 'h':
                print('p  : Practice password')
                print('l  : List passwords')
                print('n  : New password')
                print('d  : Delete password')
                print('e  : Edit password')
                print('f  : Set password frequency')
                print('g  : Toggle password group')
                print('ga : Add password to group')
                print('gr : Remove password from group')
                print('t  : Toggle active flag')
                print('cy : Show password cycles')
                print('bf : Bruteforce false positives')
                print('q  : Quit')
                continue

            if cmd.startswith('p'):
                arg = cmd[1:].strip()
                if arg == '-':
                    pw = self.list_passwords('Practice which password? ')
                    if pw:
                        self.practice_one(pw[0])
                elif arg == 'd':
                    cdate, counter = self.start_cycle()

                    if self.practice(self.get_cycle_passwords(counter), once=True):
                        cycle_stats = self.stats['cycle']
                        cycle_stats['last_complete'] = cdate
                        cycle_stats['counter'] = counter
                        self.save_stats()
                else:
                    self.practice(self.filter_password_arg(arg, active=True))

            elif cmd.startswith('n'):
                name = input('name? ')
                salt = b2a_hex(os.urandom(16)).decode('ascii')
                key = read_verify_password(salt, ITERATIONS, 20)

                # Why only save 3 hex digits? We're not actually trying to validate the
                # password is correct, just to inform the user if a typo is made. If someone
                # tries to brute-force the password, then 1 in 4096 will be a false positive,
                # but all but one of those will be incorrect.

                statdict = {}
                self.stats['passwords'][name] = statdict
                self.passwds[name] = Password(name, {'iterations': ITERATIONS, 'salt': salt, 'key': key[:3]}, statdict)
                self.save()

            elif cmd.startswith('d'):
                passwds = self.list_passwords('Delete which password? ')
                if passwds is not None:
                    for pw in passwds:
                        del self.passwds[pw.name]
                        del self.stats['passwords'][pw.name]
                        print('deleted %s' % pw.name)
                        self.save()

            elif cmd.startswith('e'):
                passwds = self.list_passwords('Edit which password? ')
                if passwds is not None:
                    for pw in passwds:
                        salt = b2a_hex(os.urandom(16)).decode('ascii')
                        key = read_verify_password(salt, ITERATIONS, 20)
                        pw.salt = salt
                        pw.key = key[:3]
                        pw.iterations = ITERATIONS
                    self.save()

            elif cmd.startswith('t'):
                while True:
                    pwl = self.list_passwords('Activate/Deactivate which password? ')
                    if pwl is None:
                        break
                    for pw in pwl:
                        pw.active = not pw.active
                        print('%s %s' % (('activated' if pw.active else 'deactivated'), pw.name))
                        self.save()

            elif cmd.startswith('bf'):
                pwl = self.list_passwords('Bruteforce which password? ')
                if pwl is None:
                    continue
                self.bruteforce_password(pwl[0])

            elif cmd.startswith('g'):
                newval = None
                txt = 'Toggle'
                if cmd.startswith('ga'):
                    newval = True
                    txt = 'Add'
                elif cmd.startswith('gr'):
                    newval = False
                    txt = 'Remove'

                group = input('Group name: ')
                if group:
                    while True:
                        pwl = self.list_passwords('%s group "%s" for which password? ' % (txt, group))
                        if pwl is None:
                            break
                        for pw in pwl:
                            ingroup = group in pw.groups
                            if ingroup != newval:
                                try:
                                    pw.groups.remove(group)
                                except ValueError:
                                    pw.groups.append(group)
                                    pw.groups.sort()

                        self.save()

            elif cmd.startswith('cy'):
                self.show_cycle()

            elif cmd.startswith('f'):
                try:
                    freq = int(input('Frequency: '))
                except ValueError:
                    print('Invalid number')
                else:
                    while True:
                        pwl = self.list_passwords('Set frequency = %d for which password? ' % freq)
                        if pwl is None:
                            break

                        for pw in pwl:
                            pw.freq = freq
                            pw.cycle = 0 if freq == 1 else -1

                    self.rebalance_passwords()
                    self.save()

            elif cmd.startswith('l'):
                arg = cmd[1:].strip()
                self.list_passwords(passwords=self.filter_password_arg(arg))

            elif cmd.startswith('q'):
                return

    def filter_password_arg(self, arg, active=None):
        groups = None
        if arg:
            groups = arg

        return self.filter_passwords(active=active, groups=groups)

    def filter_passwords(self, active=None, groups=None):
        all_passwords = list(self.passwds.values())
        if active is not None:
            all_passwords = [v for v in all_passwords if v.active]

        if groups is not None:
            exp = tagexpr.parse_tag_expr_and(groups)
            all_passwords = [v for v in all_passwords if exp.run(v.groups)]

        return all_passwords

    def practice(self, all_passwords, once=False):
        print()
        print('Password practice, enter blank password to stop')

        if not all_passwords:
            print('No matching passwords!')
            return

        while True:
            print('Shuffling passwords...')
            random.shuffle(all_passwords)
            for pw in all_passwords:
                if not self.practice_one(pw):
                    return False

            if once:
                return True

    def practice_one(self, pw):
        salt = pw.salt
        iterations = pw.iterations
        key = pw.key
        while True:
            key = read_hashed_password('%s: ' % pw.name, salt, iterations, 2)
            if key is None:
                return False

            if pw.key.lower() == key[:3].lower():
                self.record_stats(pw, {'pass_count': 1})
                print('ok!')

                return True
            self.record_stats(pw, {'fail_count': 1})
            print('fail!')


def main():
    if sys.platform == 'win32':
        xdg_config = os.getenv('APPDATA') or expanduser('~/AppData/Roaming')
    else:
        xdg_config = os.getenv('XDG_CONFIG_HOME') or expanduser('~/.config')

    default_config_path = join(xdg_config, 'ktpanda.org', 'BPM')

    p = argparse.ArgumentParser(description='')
    p.add_argument('-v', '--version', action='store_true', help='Print version information and exit')
    p.add_argument('-s', '--stats', action='store_true', help='Keep individual stats for each password')
    p.add_argument('-p', '--path', default=default_config_path, help='Directory to store configuration in (default: %s)' % default_config_path)
    args = p.parse_args()

    if args.version:
        print(VERSION)
        return 0

    try:
        os.makedirs(args.path)
    except FileExistsError:
        pass

    pp = PasswordPractice(args.path, stats=args.stats)
    pp.load()
    pp.menu()

if __name__ == '__main__':
    main()
