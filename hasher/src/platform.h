
#ifdef unix

#include <unistd.h>
#include <termios.h>
#include <sys/mman.h>
#include <pthread.h>

#define MUTEX_TYPE pthread_mutex_t
#define THREAD_TYPE pthread_t
#define MUTEX_INIT(m) pthread_mutex_init(&(m), NULL)
#define MUTEX_LOCK(m) pthread_mutex_lock(&(m))
#define MUTEX_UNLOCK(m) pthread_mutex_unlock(&(m))
#define THREAD_DECL

typedef struct {
    struct termios orig_attr, new_attr;
} getpass_ctx;

#elif defined(WIN32)

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <io.h>

#define THREAD_TYPE unsigned
#define MUTEX_TYPE CRITICAL_SECTION
#define MUTEX_INIT(m) InitializeCriticalSection(&(m))
#define MUTEX_LOCK(m) EnterCriticalSection(&(m))
#define MUTEX_UNLOCK(m) LeaveCriticalSection(&(m))
#define THREAD_DECL __stdcall

typedef struct {
    int dummy;
} getpass_ctx;

#endif

int getpass_setup_tty(int ttyfd, getpass_ctx* ctx);
int getpass_release_tty(int ttyfd, getpass_ctx* ctx);
int xwrite(int fd, const char* data, int len);
void tty_writech(int ttyfd, char ch);
int tty_readch(int ttyfd, char* ch);
void clearmem(void* mem, int len);
void lock_mem(void* base, int length);

#ifdef BUILD_BRUTEFORCER
int create_thread(THREAD_TYPE* out, void* (THREAD_DECL *func)(void*), void* data);
int get_num_cores();
int gen_random(unsigned char* buf, int len);
#endif
