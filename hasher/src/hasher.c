#include "platform.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <sys/types.h>

#define MAXPASS 256
#define MAXSALT 256
#define MAXKEY 128
#define MAXKEY_BF 4

#ifdef __GNUC__
#define NOINLINE __attribute__((noinline))
#define COMPILER_VERSION "GCC " __VERSION__

#else
#define NOINLINE
#define COMPILER_VERSION "UNKNOWN"
#endif

#ifndef BUILD_DATE
#define BUILD_DATE UNKNOWN
#endif

#ifndef SOURCE_DATE_EPOCH
#define SOURCE_DATE_EPOCH UNKNOWN
#endif

#define _str1(x) #x
#define _str(x) _str1(x)

/* Make sure buildid starts with a NUL so that "strings" never prepends anything when it prints this string. */
static const char buildid[] = "\0Built on " _str(BUILD_DATE) " (SOURCE_DATE_EPOCH=" _str(SOURCE_DATE_EPOCH) ") using " COMPILER_VERSION;

static const char hexdigits[] = "0123456789ABCDEF";
/* BEGIN SHA1 */

/*
Based on:

SHA-1 in C
By Steve Reid <steve@edmweb.com>
100% Public Domain
Test Vectors (from FIPS PUB 180-1)
"abc"
  A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
  84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1
A million repetitions of "a"
  34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F

CHANGES:

* Removed support for hashing more than 256MB (not necessary for doing HMAC with PBKDF2,
* Made SHA1Final more efficient

*/

#define SHA1_BLOCKSIZE 64
#define SHA1_OUTPUTSIZE 20

#define HMAC_SHA1_COPY(dst, src)                \
    dst.state[0] = src.state[0];                \
    dst.state[1] = src.state[1];                \
    dst.state[2] = src.state[2];                \
    dst.state[3] = src.state[3];                \
    dst.state[4] = src.state[4];                \
    dst.count = src.count;

typedef struct
{
    uint32_t state[5];
    uint32_t count;
    unsigned char buffer[64];
} SHA1_CTX;

#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

/* blk0() and blk() perform the initial expand. */
/* I got the idea of expanding during the round function from SSLeay */
#if BYTE_ORDER == LITTLE_ENDIAN
#define blk0(i) (block->l[i] = (rol(block->l[i],24)&0xFF00FF00) \
                 |(rol(block->l[i],8)&0x00FF00FF))
#elif BYTE_ORDER == BIG_ENDIAN
#define blk0(i) block->l[i]
#else
#error "Endianness not defined!"
#endif
#define blk(i) (block->l[i&15] = rol(block->l[(i+13)&15]^block->l[(i+8)&15] \
                                     ^block->l[(i+2)&15]^block->l[i&15],1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk0(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R1(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R2(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0x6ED9EBA1+rol(v,5);w=rol(w,30);
#define R3(v,w,x,y,z,i) z+=(((w|x)&y)|(w&x))+blk(i)+0x8F1BBCDC+rol(v,5);w=rol(w,30);
#define R4(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0xCA62C1D6+rol(v,5);w=rol(w,30);


/* Hash a single 512-bit block. This is the core of the algorithm. */

void SHA1Transform(
    uint32_t state[5],
    unsigned char buffer[64]
    )
{
    uint32_t a, b, c, d, e;

    typedef union
    {
        unsigned char c[64];
        uint32_t l[16];
    } CHAR64LONG16;

    CHAR64LONG16 *block = (CHAR64LONG16 *) buffer;

    /* Copy context->state[] to working vars */
    a = state[0];
    b = state[1];
    c = state[2];
    d = state[3];
    e = state[4];
    /* 4 rounds of 20 operations each. Loop unrolled. */
    R0(a, b, c, d, e, 0);
    R0(e, a, b, c, d, 1);
    R0(d, e, a, b, c, 2);
    R0(c, d, e, a, b, 3);
    R0(b, c, d, e, a, 4);
    R0(a, b, c, d, e, 5);
    R0(e, a, b, c, d, 6);
    R0(d, e, a, b, c, 7);
    R0(c, d, e, a, b, 8);
    R0(b, c, d, e, a, 9);
    R0(a, b, c, d, e, 10);
    R0(e, a, b, c, d, 11);
    R0(d, e, a, b, c, 12);
    R0(c, d, e, a, b, 13);
    R0(b, c, d, e, a, 14);
    R0(a, b, c, d, e, 15);
    R1(e, a, b, c, d, 16);
    R1(d, e, a, b, c, 17);
    R1(c, d, e, a, b, 18);
    R1(b, c, d, e, a, 19);
    R2(a, b, c, d, e, 20);
    R2(e, a, b, c, d, 21);
    R2(d, e, a, b, c, 22);
    R2(c, d, e, a, b, 23);
    R2(b, c, d, e, a, 24);
    R2(a, b, c, d, e, 25);
    R2(e, a, b, c, d, 26);
    R2(d, e, a, b, c, 27);
    R2(c, d, e, a, b, 28);
    R2(b, c, d, e, a, 29);
    R2(a, b, c, d, e, 30);
    R2(e, a, b, c, d, 31);
    R2(d, e, a, b, c, 32);
    R2(c, d, e, a, b, 33);
    R2(b, c, d, e, a, 34);
    R2(a, b, c, d, e, 35);
    R2(e, a, b, c, d, 36);
    R2(d, e, a, b, c, 37);
    R2(c, d, e, a, b, 38);
    R2(b, c, d, e, a, 39);
    R3(a, b, c, d, e, 40);
    R3(e, a, b, c, d, 41);
    R3(d, e, a, b, c, 42);
    R3(c, d, e, a, b, 43);
    R3(b, c, d, e, a, 44);
    R3(a, b, c, d, e, 45);
    R3(e, a, b, c, d, 46);
    R3(d, e, a, b, c, 47);
    R3(c, d, e, a, b, 48);
    R3(b, c, d, e, a, 49);
    R3(a, b, c, d, e, 50);
    R3(e, a, b, c, d, 51);
    R3(d, e, a, b, c, 52);
    R3(c, d, e, a, b, 53);
    R3(b, c, d, e, a, 54);
    R3(a, b, c, d, e, 55);
    R3(e, a, b, c, d, 56);
    R3(d, e, a, b, c, 57);
    R3(c, d, e, a, b, 58);
    R3(b, c, d, e, a, 59);
    R4(a, b, c, d, e, 60);
    R4(e, a, b, c, d, 61);
    R4(d, e, a, b, c, 62);
    R4(c, d, e, a, b, 63);
    R4(b, c, d, e, a, 64);
    R4(a, b, c, d, e, 65);
    R4(e, a, b, c, d, 66);
    R4(d, e, a, b, c, 67);
    R4(c, d, e, a, b, 68);
    R4(b, c, d, e, a, 69);
    R4(a, b, c, d, e, 70);
    R4(e, a, b, c, d, 71);
    R4(d, e, a, b, c, 72);
    R4(c, d, e, a, b, 73);
    R4(b, c, d, e, a, 74);
    R4(a, b, c, d, e, 75);
    R4(e, a, b, c, d, 76);
    R4(d, e, a, b, c, 77);
    R4(c, d, e, a, b, 78);
    R4(b, c, d, e, a, 79);
    /* Add the working vars back into context.state[] */
    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;
    state[4] += e;
    /* Wipe variables */
    a = b = c = d = e = 0;
}


/* SHA1Init - Initialize new context */

void SHA1Init(
    SHA1_CTX * context
    )
{
    /* SHA1 initialization constants */
    context->state[0] = 0x67452301;
    context->state[1] = 0xEFCDAB89;
    context->state[2] = 0x98BADCFE;
    context->state[3] = 0x10325476;
    context->state[4] = 0xC3D2E1F0;
    context->count = 0;
}


/* Run your data through this. */

void SHA1Update(
    SHA1_CTX * context,
    unsigned char *data,
    uint32_t len
    )
{
    uint32_t i;

    uint32_t j;

    j = context->count;
    context->count = j + len;
    j &= 63;
    if ((j + len) > 63)
    {
        memcpy(&context->buffer[j], data, (i = 64 - j));
        SHA1Transform(context->state, context->buffer);
        for (; i + 63 < len; i += 64)
        {
            memcpy(&context->buffer[0], &data[i], 64);
            SHA1Transform(context->state, context->buffer);
        }
        j = 0;
    }
    else
        i = 0;
    memcpy(&context->buffer[j], &data[i], len - i);
}


/* Add padding and return the message digest. */

void SHA1Final(
    uint32_t digest[5],
    SHA1_CTX * context
    )
{
    unsigned int j;

    j = context->count & 63;

    context->buffer[j++] = 0x80;
    if (j <= 56)
    {
        /* pad within current block */
        memset(context->buffer + j, 0, 60 - j);
    }
    else
    {
        /* pad within next block */
        memset(context->buffer + j, 0, 64 - j);
        SHA1Transform(context->state, context->buffer);
        memset(context->buffer, 0, 60);
    }

    j = (context->count << 3);
    context->buffer[60] = (j >> 24) & 0xFF;
    context->buffer[61] = (j >> 16) & 0xFF;
    context->buffer[62] = (j >> 8) & 0xFF;
    context->buffer[63] = (j) & 0xFF;

    SHA1Transform(context->state, context->buffer);
#if BYTE_ORDER == LITTLE_ENDIAN
#define SWAP(k) digest[k] = (context->state[k] >> 16) | (context->state[k] << 16); digest[k] = ((digest[k] >> 8) & 0x00FF00FF) | ((digest[k] << 8) & 0xFF00FF00)
    SWAP(0);
    SWAP(1);
    SWAP(2);
    SWAP(3);
    SWAP(4);
#else
    memcpy(digest, context->state, 20);
#endif
}

/* END SHA1 */

/******************************
 * PBKDF2
 ******************************/

/* Put all of our context into a big buffer where we can easily clear it */

typedef struct {
    SHA1_CTX hash_inner;
    SHA1_CTX hash_outer;
    SHA1_CTX hash_temp;

    int len_passwd, len_salt;

    unsigned char hmac_key_inner[SHA1_BLOCKSIZE];
    unsigned char hmac_key_outer[SHA1_BLOCKSIZE];
    unsigned char hmac_temp[SHA1_OUTPUTSIZE];

    unsigned char u[SHA1_OUTPUTSIZE];
    unsigned char rv[SHA1_OUTPUTSIZE];

    unsigned char block_byte[4];

    unsigned char passwd[MAXPASS];
    unsigned char salt[MAXSALT];
} PBKDF2_CTX;

static NOINLINE void _SHA1_pbkdf2_digest(PBKDF2_CTX* ctx, int iterations, int keylen, unsigned char* keyout) {
    int i, num_blocks, block;

    memset(ctx->hmac_key_inner, 0, SHA1_BLOCKSIZE);

    /* Init HMAC keys and save their hash states */

    if (ctx->len_passwd > SHA1_BLOCKSIZE) {
        SHA1Init(&ctx->hash_temp);
        SHA1Update(&ctx->hash_temp, ctx->passwd, ctx->len_passwd);
        SHA1Final((uint32_t*)ctx->hmac_key_inner, &ctx->hash_temp);
    } else {
        memcpy(ctx->hmac_key_inner, ctx->passwd, ctx->len_passwd);
    }

    for (i = 0; i < SHA1_BLOCKSIZE; i++) {
        ctx->hmac_key_outer[i] = ctx->hmac_key_inner[i] ^ 0x5c;
        ctx->hmac_key_inner[i] ^= 0x36;
    }

    SHA1Init(&ctx->hash_inner);
    SHA1Update(&ctx->hash_inner, ctx->hmac_key_inner, SHA1_BLOCKSIZE);

    SHA1Init(&ctx->hash_outer);
    SHA1Update(&ctx->hash_outer, ctx->hmac_key_outer, SHA1_BLOCKSIZE);

    num_blocks = (keylen + SHA1_OUTPUTSIZE - 1) / SHA1_OUTPUTSIZE;

    for (block = 1; block <= num_blocks; block++) {
        int iter, j, copypos, copylen;

        HMAC_SHA1_COPY(ctx->hash_temp, ctx->hash_inner);
        SHA1Update(&ctx->hash_temp, ctx->salt, ctx->len_salt);
        ctx->block_byte[0] = (block >> 24) & 0xFF;
        ctx->block_byte[1] = (block >> 16) & 0xFF;
        ctx->block_byte[2] = (block >> 8) & 0xFF;
        ctx->block_byte[3] = (block) & 0xFF;
        SHA1Update(&ctx->hash_temp, ctx->block_byte, 4);
        SHA1Final((uint32_t*)ctx->hmac_temp, &ctx->hash_temp);

        HMAC_SHA1_COPY(ctx->hash_temp, ctx->hash_outer);
        SHA1Update(&ctx->hash_temp, ctx->hmac_temp, SHA1_OUTPUTSIZE);
        SHA1Final((uint32_t*)ctx->u, &ctx->hash_temp);

        memcpy(ctx->rv, ctx->u, SHA1_OUTPUTSIZE);

        for (iter = 1; iter < iterations; iter++) {
            HMAC_SHA1_COPY(ctx->hash_temp, ctx->hash_inner);
            SHA1Update(&ctx->hash_temp, ctx->u, SHA1_OUTPUTSIZE);
            SHA1Final((uint32_t*)ctx->hmac_temp, &ctx->hash_temp);

            HMAC_SHA1_COPY(ctx->hash_temp, ctx->hash_outer);
            SHA1Update(&ctx->hash_temp, ctx->hmac_temp, SHA1_OUTPUTSIZE);
            SHA1Final((uint32_t*)ctx->u, &ctx->hash_temp);

            for (j = 0; j < SHA1_OUTPUTSIZE; j++) {
                ctx->rv[j] ^= ctx->u[j];
            }
        }

        copypos = (block - 1) * SHA1_OUTPUTSIZE;
        copylen = keylen - copypos;
        if (copylen > SHA1_OUTPUTSIZE)
            copylen = SHA1_OUTPUTSIZE;
        memcpy(keyout + copypos, ctx->rv, copylen);
    }
}

int parse_int(const char* txt, int* rval) {
    char* end;
    if (*txt == 0)
        return 0;
    *rval = strtol(txt, &end, 0);
    return *end == 0;
}

int parse_uint64(const char* txt, uint64_t* rval) {
    char* end;
    if (*txt == 0)
        return 0;
    *rval = strtoull(txt, &end, 0);
    return *end == 0;
}

static int parse_hex(const char* hex, unsigned char* bin, int *outlen, int maxlen) {
    int i;
    int length = strlen(hex);

    if ((*outlen = (length + 1) >> 1) > maxlen)
        return 0;

    for (i = 0; i < length; i++) {
        char c = *hex++;
        unsigned char v = 0;
        if ('0' <= c && c <= '9') {
            v = c - '0';
        } else if ('a' <= c && c <= 'f') {
            v = c - 'a' + 10;
        } else if ('A' <= c && c <= 'F') {
            v = c - 'A' + 10;
        } else {
            return 0;
        }
        if ((i & 1) == 0) {
            bin[i >> 1] = v << 4;
        } else {
            bin[i >> 1] |= v;
        }
    }
    return 1;
}



/******************************
 * Password reading
 ******************************/

static void erasechars(int ttyfd, int len) {
    int i;
    for (i = 0; i < len; i++) {
        tty_writech(ttyfd, 8);
    }
    for (i = 0; i < len; i++) {
        tty_writech(ttyfd, ' ');
    }
    for (i = 0; i < len; i++) {
        tty_writech(ttyfd, 8);
    }
}

static int _readpass(int ttyfd, getpass_ctx* ctx, char* passw, int* pwlen, int istty) {
    char c;
    int len = 0;
    int rc = 0;

    while (1) {
        int nr = istty ? tty_readch(ttyfd, &c) : read(ttyfd, &c, 1);
        if (nr < 0) {
            rc = -1;
            break;
        }
        if (nr == 0 || c == 4) {
            rc = -2;
            break;
        }
        if (c == 3) {
            rc = -3;
            break;
        }

        if (c == 10 || c == 13)
            break;

        if (c == 8 || c == 127) {
            if (len > 0) {
                len--;
                if (istty)
                    erasechars(ttyfd, 1);
            }
        } else {
            if (len < MAXPASS) {
                passw[len++] = c;
                if (istty)
                    tty_writech(ttyfd, '*');
            }
        }
    }
    if (istty)
        erasechars(ttyfd, len);
    *pwlen = len;
    if (rc == 0 && len == 0)
        return -4;
    return rc;
}

static int _getpass(int ttyfd, PBKDF2_CTX* ctx, int istty) {
    int rc;
    getpass_ctx c;
    if (istty)
        getpass_setup_tty(ttyfd, &c);
    rc = _readpass(ttyfd, &c, (char*)ctx->passwd, &ctx->len_passwd, istty);
    if (istty) {
        getpass_release_tty(ttyfd, &c);
        tty_writech(ttyfd, '\r');
        tty_writech(ttyfd, '\n');
    }
    if (rc != 0) {
        if (rc == -1) {
            rc = errno;
            if (rc == 0) rc = EINVAL;
        }
    }

    return rc;
}





/******************************
 * Bruteforcer
 ******************************/

#ifdef BUILD_BRUTEFORCER

#define MAXTHREADS 64

typedef struct {
    MUTEX_TYPE seq_lock;
    MUTEX_TYPE output_lock;

    const char* bruteforce_chars;
    int len_chars;
    int entropy_len;

    PBKDF2_CTX base_ctx;
    PBKDF2_CTX alt_base_ctx;

    uint64_t count;

    int iterations;
    int keybits;
    int pwl;
    uint64_t start;

    int found_result;

    unsigned char entropy[256];
    unsigned char expect_key_bin[MAXKEY_BF];
    uint8_t bruteforce_index[MAXPASS];
} bruteforce_ctx;

typedef struct {
    THREAD_TYPE id;
    bruteforce_ctx* common;
    PBKDF2_CTX ctx1;
    PBKDF2_CTX ctx2;
    unsigned char key1[MAXKEY_BF];
    unsigned char key2[MAXKEY_BF];

} thread_info;

struct {
    thread_info threads[MAXTHREADS];
    bruteforce_ctx bfctx;
} sensitive_data;

void* THREAD_DECL bruteforce_thread(void* arg) {
    int carry, j;
    uint64_t count;
    int ok;
    thread_info* cl = (thread_info*)arg;
    bruteforce_ctx* c = cl->common;
    PBKDF2_CTX* ctx = &cl->ctx1;
    int keylen = (c->keybits + 7) >> 3;
    int keymask = 0xFF;
    int keymaskbyte = 0;

    if ((c->keybits & 7) != 0) {
        keymask = 0xFF << (8 - (c->keybits & 7));
        keymaskbyte = c->keybits >> 3;
    }
    memcpy(ctx, &c->base_ctx, sizeof(PBKDF2_CTX));

    while (1) {
        MUTEX_LOCK(c->seq_lock);

        if (c->pwl >= MAXPASS-1) {
            MUTEX_UNLOCK(c->seq_lock);
            return NULL;
        }

        carry = 1;
        for (j = 0; j < c->pwl; j++) {
            uint8_t v = c->bruteforce_index[j];
            ctx->passwd[c->pwl - j - 1] = c->bruteforce_chars[v];
            v += carry;
            carry = v >= c->len_chars ? 1 : 0;
            if (carry) {
                v -= c->len_chars;
            }
            c->bruteforce_index[j] = v;
        }
        if (carry) {
            c->pwl++;
            if (c->pwl >= MAXPASS-1) {
                MUTEX_UNLOCK(c->seq_lock);
                return NULL;
            }
            for (j = 0; j < c->pwl; j++) {
                c->bruteforce_index[j] = 0;
            }

        }

        ctx->len_passwd = c->pwl;

        count = c->count++;

        MUTEX_UNLOCK(c->seq_lock);

        _SHA1_pbkdf2_digest(ctx, c->iterations, keylen, cl->key1);
        cl->key1[keymaskbyte] &= keymask;

        ok = (memcmp(cl->key1, c->expect_key_bin, keylen) == 0);
        ctx->passwd[ctx->len_passwd] = 0;

        MUTEX_LOCK(c->output_lock);

        if (ok) {
            printf("\r>%16llu %-32s\n", count, ctx->passwd);
            fflush(stdout);
        } else {
            printf("\r %16llu %-32s\r", count, ctx->passwd);
            fflush(stdout);
        }

        MUTEX_UNLOCK(c->output_lock);
    }
}

void* THREAD_DECL bruteforce_match_thread(void* arg) {
    int j;
    uint64_t count;
    int ok;
    thread_info* cl = (thread_info*)arg;
    bruteforce_ctx* c = cl->common;
    PBKDF2_CTX* ctx1 = &cl->ctx1;
    PBKDF2_CTX* ctx2 = &cl->ctx2;
    int keylen = (c->keybits + 7) >> 3;
    int keymask = 0xFF;
    int keymaskbyte = 0;
    if ((c->keybits & 7) != 0) {
        keymask = 0xFF << (8 - (c->keybits & 7));
        keymaskbyte = c->keybits >> 3;
    }

    memcpy(ctx1, &c->base_ctx, sizeof(PBKDF2_CTX));
    memcpy(ctx2, &c->alt_base_ctx, sizeof(PBKDF2_CTX));

    ctx1->len_salt = 16;
    ctx2->len_salt = 16;
    while (1) {
        MUTEX_LOCK(c->seq_lock);
        if (c->entropy_len < 16) {
            if (gen_random(c->entropy, 256)) {
                MUTEX_UNLOCK(c->seq_lock);
                return NULL;
            }
            c->entropy_len = 256;
        }

        c->entropy_len -= 16;
        memcpy(ctx1->salt, &c->entropy[c->entropy_len], 16);

        count = c->count++;

        MUTEX_UNLOCK(c->seq_lock);

        memcpy(ctx2->salt, ctx1->salt, 16);

        _SHA1_pbkdf2_digest(ctx1, c->iterations, keylen, cl->key1);
        _SHA1_pbkdf2_digest(ctx2, c->iterations, keylen, cl->key2);
        cl->key1[keymaskbyte] &= keymask;
        cl->key2[keymaskbyte] &= keymask;

        ok = (memcmp(cl->key1, cl->key2, keylen) == 0);
        MUTEX_LOCK(c->output_lock);

        if (c->found_result) {
            MUTEX_UNLOCK(c->output_lock);
            return NULL;
        }

        if (ok) {
            c->found_result = 1;

            fprintf(stderr, "\r>%16llu\n", count);
            fflush(stderr);
            printf("s:");
            for (j = 0; j < 16; j++) {
                printf("%02X", ctx1->salt[j]);
            }
            printf("\nk:");
            for (j = 0; j < keylen; j++) {
                printf("%02X", cl->key1[j]);
            }
            printf("\n");
            fflush(stdout);
            MUTEX_UNLOCK(c->output_lock);
            return NULL;
        } else {
            fprintf(stderr, "\r %16llu\r", count);
            fflush(stderr);
        }

        MUTEX_UNLOCK(c->output_lock);
    }
}

int main(int argc, char** argv) {

    const char* expect_key;
    int expect_key_len;
    int nthreads;
    int j, rc;
    int gen;
    uint64_t num_comb, base_count;
    bruteforce_ctx* c = &sensitive_data.bfctx;
    PBKDF2_CTX* ctx = &c->base_ctx;
    PBKDF2_CTX* ctx2 = &c->alt_base_ctx;

    lock_mem(&sensitive_data, sizeof(sensitive_data));
    clearmem(&sensitive_data, sizeof(sensitive_data));

    gen = argc >= 2 && strcmp(argv[1], "gen") == 0;
    if (argc < (gen ? 4 : 6)) {
        fprintf(stderr, "BPM bruteforce assistant. %s\n", buildid + 1);
        fprintf(stderr, "\n");
        fprintf(stderr, "usage: %s salt key keybits iterations chars [threads] [start]\n", argv[0]);
        fprintf(stderr, "    or %s \"gen\" keybits iterations [threads]\n", argv[0]);
        return 1;
    }

    j = 1;

    if (gen) {
        j++;

        if (!parse_int(argv[j], &c->keybits) || c->keybits < 0 || c->keybits > (MAXKEY_BF * 8)) {
            fprintf(stderr, "Invalid value for keybits: %s\n", argv[j]);
            return 1;
        }

        j++;

        if (!parse_int(argv[j], &c->iterations) || c->iterations < 1) {
            fprintf(stderr, "Invalid value for iterations: %s\n", argv[j]);
            return 1;
        }

        j++;

    } else {

        if (!parse_hex(argv[j], ctx->salt, &ctx->len_salt, MAXSALT)) {
            fprintf(stderr, "Invalid value for salt: %s\n", argv[j]);
            return 1;
        }

        j++;

        expect_key = argv[j];
        if (!parse_hex(argv[j], c->expect_key_bin, &expect_key_len, MAXKEY_BF)) {
            fprintf(stderr, "Invalid value for key: %s\n", argv[j]);
            return 1;
        }

        j++;

        if (!parse_int(argv[j], &c->keybits) || c->keybits < 0 || c->keybits > (strlen(expect_key) * 4)) {
            fprintf(stderr, "Invalid value for keybits: %s\n", argv[j]);
            return 1;
        }

        j++;
        if (!parse_int(argv[j], &c->iterations) || c->iterations < 1) {
            fprintf(stderr, "Invalid value for iterations: %s\n", argv[j]);
            return 1;
        }

        j++;
        c->bruteforce_chars = argv[j];
        c->len_chars = strlen(c->bruteforce_chars);
        if (c->len_chars == 0) {
            fprintf(stderr, "Invalid value for chars: empty string\n");
            return 1;
        }
    }

    if (++j < argc) {
        if (!parse_int(argv[j], &nthreads) || nthreads < 0 || nthreads > MAXTHREADS) {
            fprintf(stderr, "Invalid value for threads: %s\n", argv[j]);
            return 1;
        }
    }

    if (nthreads == 0) {
        nthreads = get_num_cores();
        if (nthreads <= 0) {
            nthreads = 4;
        }
        if (nthreads > MAXTHREADS)
            nthreads = MAXTHREADS;
    }

    if (!gen) {
        if (++j < argc) {
            if (!parse_uint64(argv[j], &c->start) || c->start < 0) {
                fprintf(stderr, "Invalid value for start: %s\n", argv[j]);
                return 1;
            }

        }
    }

    if (gen) {
        int istty = isatty(0);
        while(1) {
            if (istty) {
                fprintf(stderr, "new password: ");
                fflush(stderr);
            }
            rc = _getpass(0, ctx, istty);
            if (rc != 0)
                break;

            if (!istty)
                break;

            fprintf(stderr, "verify: ");
            fflush(stderr);
            rc = _getpass(0, ctx2, istty);
            if (rc != 0)
                break;

            if (ctx->len_passwd != ctx2->len_passwd || memcmp(ctx->passwd, ctx2->passwd, ctx->len_passwd) != 0) {
                fprintf(stderr, "passwords do not match\n");
                continue;
            }
            break;
        }
        if (rc == 0) {
            if (istty) {
                fprintf(stderr, "alternate: ");
                fflush(stderr);
            }
            rc = _getpass(0, ctx2, istty);
        }
        if (rc > 0) {
            errno = rc;
            perror("getpass");
            return 1;
        }
        if (rc < 0) {
            if (rc == -2) {
                fprintf(stderr, "eof\n");
            } else if (rc == -3) {
                fprintf(stderr, "interrupted\n");
            } else {
                fprintf(stderr, "unknown: %d\n", rc);
            }
            return 1;
        }

    } else {
        c->pwl = 1;
        base_count = 0;
        num_comb = c->len_chars;
        while((base_count + num_comb) < c->start) {
            c->pwl++;
            base_count += num_comb;
            num_comb *= c->len_chars;
        }
        base_count = c->start - base_count;
        for (j = 0; j < c->pwl; j++) {
            c->bruteforce_index[j] = base_count % c->len_chars;
            base_count /= c->len_chars;
        }

        c->count = c->start;

    }
    if ((c->keybits & 7) != 0) {
        int keymask = 0xFF << (8 - (c->keybits & 7));
        int keymaskbyte = c->keybits >> 3;
        c->expect_key_bin[keymaskbyte] &= keymask;
    }

    MUTEX_INIT(c->seq_lock);
    MUTEX_INIT(c->output_lock);

    for(j = 0; j < (nthreads - 1); j++) {
        sensitive_data.threads[j].common = &sensitive_data.bfctx;
        if (!create_thread(&sensitive_data.threads[j].id, gen ? bruteforce_match_thread : bruteforce_thread, &sensitive_data.threads[j])) {
            nthreads = j + 1;
            break;
        }
    }

    sensitive_data.threads[nthreads - 1].common = &sensitive_data.bfctx;
    if (gen) {
        bruteforce_match_thread(&sensitive_data.threads[nthreads - 1]);
    } else {
        bruteforce_thread(&sensitive_data.threads[nthreads - 1]);
    }
    clearmem(&sensitive_data, sizeof(sensitive_data));
    return 0;
}

#else

struct {
    PBKDF2_CTX ctx;
    unsigned char key_output[MAXKEY];
} sensitive_data;

int main(int argc, char** argv) {
    PBKDF2_CTX* ctx = &sensitive_data.ctx;
    int nr, i, iterations, keylen;
    unsigned char *key = sensitive_data.key_output;

    lock_mem(&sensitive_data, sizeof(sensitive_data));
    clearmem(&sensitive_data, sizeof(sensitive_data));

    if (argc == 1) {
        char msg[] = "BPM hash assistant. Do not run directly. ";
        xwrite(1, msg, sizeof(msg) - 1);
        xwrite(1, buildid + 1, sizeof(buildid) - 2);
        xwrite(1, "\n", 1);
        return 1;
    }

    if (argc != 4) {
        xwrite(1, "e:argc\n", 7);
        return 1;
    }

    if (!parse_hex(argv[1], ctx->salt, &ctx->len_salt, MAXSALT)) {
        xwrite(1, "e:salt\n", 7);
        return 1;
    }

    if (!parse_int(argv[2], &keylen) || keylen <= 0 || keylen > MAXKEY) {
        xwrite(1, "e:keylen\n", 9);
        return 1;
    }

    if (!parse_int(argv[3], &iterations) || iterations <= 0) {
        xwrite(1, "e:iterations\n", 13);
        return 1;
    }

    nr = _getpass(0, ctx, isatty(0));
    if (nr > 0) {
        xwrite(1, "e:read\n", 7);
        return nr;
    }
    if (nr == -2) {
        xwrite(1, "e:eof\n", 6);
        return 0;
    }
    if (nr == -3) {
        xwrite(1, "e:intr\n", 7);
        return 0;
    }
    if (nr == -4) {
        xwrite(1, "e:empty\n", 8);
        return 0;
    }

    _SHA1_pbkdf2_digest(ctx, iterations, keylen, key);

    xwrite(1, "k:", 2);
    for (i = 0; i < keylen; i++) {
        xwrite(1, &hexdigits[key[i] >> 4], 1);
        xwrite(1, &hexdigits[key[i] & 15], 1);
    }
    clearmem(&sensitive_data, sizeof(sensitive_data));
    xwrite(1, "\n", 1);
    return 0;
}

#endif
