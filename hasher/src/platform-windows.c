#include "platform.h"

#include <stdio.h>
#include <process.h>
#include <conio.h>
#include <memoryapi.h>

#ifdef BUILD_BRUTEFORCER
#include <wincrypt.h>

int create_thread(THREAD_TYPE* out, void* (THREAD_DECL *func)(void*), void* data) {
    if (!_beginthreadex(NULL, 0, (unsigned int (THREAD_DECL *)(void *)) func, data, 0, out)) {
        perror("_beginthreadex");
        return 0;
    }
    return 1;
}

int get_num_cores() {
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    return sysinfo.dwNumberOfProcessors;
}

int gen_random(unsigned char* buf, int len) {
    static HCRYPTPROV hCryptProv = 0;
    if (hCryptProv == 0) {
        if (!CryptAcquireContext(&hCryptProv, NULL, NULL,
                                 PROV_RSA_FULL, CRYPT_VERIFYCONTEXT)) {
            return -1;
        }
    }
    if (!CryptGenRandom(hCryptProv, len, buf)) {
        return -1;
    }
    return 0;
}

#endif

int getpass_setup_tty(int ttyfd, getpass_ctx* ctx) {
    return 0;
}

int getpass_release_tty(int ttyfd, getpass_ctx* ctx) {
    return 0;
}

int xwrite(int fd, const char* data, int len) {
    return _write(fd, data, len);
}

void tty_writech(int ttyfd, char ch) {
    _putch(ch);
}

int tty_readch(int ttyfd, char* ch) {
    while (1) {
        char chr = _getch();
        if (chr != 0 && chr != 0xe0) {
            *ch = chr;
            return 1;
        }
    }
}

void clearmem(void* mem, int len) {
    memset(mem, 0, len);
}

void lock_mem(void* base, int len) {
    VirtualLock(base, len);
}
