#include "platform.h"
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef BUILD_BRUTEFORCER

int create_thread(pthread_t* out, void* (*func)(void*), void* data) {
    pthread_attr_t thrattr;

    pthread_attr_init(&thrattr);

    pthread_attr_setdetachstate(&thrattr, PTHREAD_CREATE_DETACHED);
    if (pthread_create(out, &thrattr, func, data)) {
        perror("pthread_create");
        return 0;
    }
    return 1;
}

int get_num_cores() {
    return sysconf(_SC_NPROCESSORS_ONLN);
}

#endif

int getpass_setup_tty(int ttyfd, getpass_ctx* ctx) {
    if (tcgetattr(ttyfd, &ctx->orig_attr) < 0) {
        return errno;
    }

    memcpy(&ctx->new_attr, &ctx->orig_attr, sizeof(ctx->new_attr));

    cfmakeraw(&ctx->new_attr);
    ctx->new_attr.c_cc[VMIN] = 1;

    if (tcsetattr(ttyfd, TCSANOW, &ctx->new_attr)) {
        return errno;
    }

    return 0;
}

int getpass_release_tty(int ttyfd, getpass_ctx* ctx) {
    tcsetattr(ttyfd, TCSANOW, &ctx->orig_attr);
    return 0;
}

int xwrite(int fd, const char* data, int len) {
    return write(fd, data, len);
}


void tty_writech(int ttyfd, char ch) {
    xwrite(ttyfd, &ch, 1);
}
int tty_readch(int ttyfd, char* ch) {
    return read(ttyfd, ch, 1);
}

int gen_random(unsigned char* buf, int len) {
    static int fd = 0;
    if (fd == 0) {
        fd = open("/dev/urandom", O_RDONLY);
    }
    if (fd < 0) {
        perror("open urandom");
        return fd;
    }

    while(len) {
        int nr = read(fd, buf, len);
        if (nr <= 0)
            return -1;
        buf += nr;
        len -= nr;
    }
    return 0;
}

void clearmem(void* mem, int len) {
    memset(mem, 0, len);
}

void lock_mem(void* base, int len) {
    mlockall(MCL_CURRENT|MCL_FUTURE);
}
