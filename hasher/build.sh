#!/bin/sh

IFS=

cd `dirname $0`

CTIME=$(date +%s)

TMP_CHECKSUMS=source_checksums-$$.txt

sha256sum Makefile build.sh src/*.c src/*.h > $TMP_CHECKSUMS

LAST_BUILD_SRC_CKSUMS="`cat source_checksums.txt 2>/dev/null`"
SRC_CKSUMS="`cat $TMP_CHECKSUMS`"

if [ "$LAST_BUILD_SRC_CKSUMS" != "$SRC_CKSUMS" ]; then
    echo "Source changes detected, new date epoch = $CTIME"
    echo
    SOURCE_DATE_EPOCH=$CTIME
    mv $TMP_CHECKSUMS source_checksums.txt
    echo $SOURCE_DATE_EPOCH > build_date_epoch.txt
else
    rm -f $TMP_CHECKSUMS
    SOURCE_DATE_EPOCH=`cat build_date_epoch.txt`
    echo "Rebuilding for date epoch $SOURCE_DATE_EPOCH"
    echo
    if [ -e binary_checksums.txt ]; then
        VERIFY=1
        mv binary_checksums.txt binary_checksums_verify.txt
    fi
fi

export SOURCE_DATE_EPOCH

make CM=1 clean
make CM=1 -j8

find bin/ -type f | LC_ALL=C sort | while read fn; do
    sha256sum $fn
    strings $fn | grep SOURCE_DATE_EPOCH=
    echo
done > binary_checksums.txt


if [ -n "$VERIFY" ]; then
    echo
    echo "Verifying build checksums"
    diff -U5 binary_checksums_verify.txt binary_checksums.txt && echo "Checksums are identical"
fi
